# MorphText

#### 介绍
安卓版文字过场控件，仿Github安卓htextview

系统要求：Java8

目前实现效果：Scale，Evaporate

用法：


```
mMorphTextview = this.findViewById( R.id.mtv_test );
mMorphTextview.MEffect = MorphTextview.MorphEffect.Scale;

mMorphTextview.morph( "'What can I do with it?'" );
mMorphTextview.morph( "Design" );
mMorphTextview.morph( "Design is not just" );
```

