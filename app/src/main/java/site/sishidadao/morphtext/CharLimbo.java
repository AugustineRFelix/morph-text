package site.sishidadao.morphtext;

import android.animation.Animator;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

import java.util.List;

abstract class CharLimbo {
    public enum LimboType {
        Enter,
        Leave,
        Shift,
    }

    public static class RandomInterpolator implements Interpolator {
        @Override
        public float getInterpolation( float v ) {
            return ( float ) Math.random( );
        }
    }

    protected MorphChar mMorphChar = null;
    public long Delay = 0;
    protected long mDuration = 0;

    public CharLimbo( MorphChar morphChar, long delay, long duration ) {
        this.mMorphChar = morphChar;
        this.Delay = delay;
        this.mDuration = duration;
    }

    public abstract void attachAnimations( List<Animator> animatiors );

    public abstract ViewGroup.LayoutParams getStartupLayoutParas( );
}


