package site.sishidadao.morphtext;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.AbsoluteLayout;

import java.util.List;

public class CharMorphLimboScale extends CharMorphLimbo {
    public CharMorphLimboScale( MorphChar morphChar, long delay, long duration,
                                ExtraInfo extraInfo ) {
        super( morphChar, delay, duration, extraInfo );
    }

    public void attachLeaveAnimations( List<Animator> animatiors ) {
        Interpolator interpolator = new AccelerateInterpolator( 1.5f );

        Animator aniScaleX = ObjectAnimator.ofFloat( mMorphChar.TextViewRef, "scaleX", 1, 0.8f );
        aniScaleX.setInterpolator( interpolator );
        aniScaleX.setDuration( mDuration );
        aniScaleX.setStartDelay( Delay );
        animatiors.add( aniScaleX );

        Animator aniScaleY = ObjectAnimator.ofFloat( mMorphChar.TextViewRef, "scaleY", 1, 0.8f );
        aniScaleY.setInterpolator( interpolator );
        aniScaleY.setDuration( mDuration );
        aniScaleY.setStartDelay( Delay );
        animatiors.add( aniScaleY );

        Animator aniAlpha = ObjectAnimator.ofFloat( mMorphChar.TextViewRef, "alpha", 1, 0 );
        aniAlpha.setInterpolator( interpolator );
        aniAlpha.setStartDelay( Delay );
        aniAlpha.setDuration( mDuration );
        animatiors.add( aniAlpha );
    }

    public void attachEnterAnimations( List<Animator> animatiors ) {
        OvershootInterpolator overshootInterpolator = new OvershootInterpolator( );
        Animator aniScaleX = ObjectAnimator.ofFloat( mMorphChar.TextViewRef, "scaleX", 1, 1.5f, 1 );
        aniScaleX.setInterpolator( overshootInterpolator );
        aniScaleX.setDuration( mDuration );
        aniScaleX.setStartDelay( Delay );
        animatiors.add( aniScaleX );

        Animator aniScaleY = ObjectAnimator.ofFloat( mMorphChar.TextViewRef, "scaleY", 1, 1.5f, 1 );
        aniScaleY.setInterpolator( overshootInterpolator );
        aniScaleY.setDuration( mDuration );
        aniScaleY.setStartDelay( Delay );
        animatiors.add( aniScaleY );

        Animator aniAlpha = ObjectAnimator.ofFloat( mMorphChar.TextViewRef, "alpha", 0, 1 );
        aniAlpha.setInterpolator( new AccelerateInterpolator( 1.5f ) );
        aniAlpha.setDuration( mDuration );
        aniAlpha.setStartDelay( Delay );
        animatiors.add( aniAlpha );
    }

    public void attachShiftAnimations( List<Animator> animatiors ) {
        int fromX = this.calXPos4FromIdx( mMorphChar.FromIdx );
        int toX = this.calXPos4ToIdx( mMorphChar.ToIdx );
        if ( fromX != toX ) {
            Animator aniXPos = ObjectAnimator.ofFloat( mMorphChar.TextViewRef, "x", fromX, toX );
            aniXPos.setDuration( mDuration );
            animatiors.add( aniXPos );
        }
    }

    public ViewGroup.LayoutParams getStartupLayoutParas( ) {
        if ( this.mMorphChar.getLimboType( ) == LimboType.Enter )
            return new AbsoluteLayout.LayoutParams(
                    this.mMorphChar.TextViewRef.getMeasuredWidth( ), this.mMorphChar.TextViewRef.getMeasuredHeight( ),
                    this.calXPos4ToIdx( mMorphChar.ToIdx ), mExtraInfo.YPos );
        else
            return this.mMorphChar.TextViewRef.getLayoutParams( );
    }
}
