package site.sishidadao.morphtext;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button mBtnTest;
    private MorphTextview mMorphTextview;
    private TextView mTvTest;

    private String[] mLabels = new String[]{
            "为什么有时候会存在遮挡呢？", "英文的显示效果会好一些", "因为英文很容易存在重复字", "1 张 三", "1 张 三 一号台", "1 张 三",
            "Older people", "sit down and ask,", "'What is it?'", "but the boy asks,", "'What can I do with it?'", "- Steve Jobs",
            "Design", "Design is not just", "What it looks like", "and feels like.", "Design", "is how it works.", "- Steve Jobs",
            "Swift", "Objective-C", "IPhone", "IPad", "Mac Mini", "MacBook Pro", "Mac Pro",
            //"动画的效果不能阻断", "否则会引起闪烁", "解决的办法是动画队列", "还有很多问题需要解决","任天堂Switch", "破解版的3ds", "ps4破解了有什么用呢？",
            //"英文的显示效果会好一些", "因为英文很容易存在重复字", "1 张 三", "1 张 三 一号台"
    };

    //    private String[] mLabels = new String[]{
//            "皇室战争力量哥虎牙",
//            "停播 皇室战争，接下来．．．5", "停播 皇室战争，接下来．．．4", "停播 皇室战争，接下来．．．3",
//            "停播 皇室战争，接下来．．．2", "停播 皇室战争，接下来．．．1",
//    };
    private int mLabelIdx = 0;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        mTvTest = this.findViewById( R.id.tv_test );

        mMorphTextview = this.findViewById( R.id.mtv_test );
        mMorphTextview.MEffect = MorphTextview.MorphEffect.Scale;

        mBtnTest = this.findViewById( R.id.btn_test );
        mBtnTest.setOnClickListener( ( v ) -> {
            mMorphTextview.morph( mLabels[ mLabelIdx++ % mLabels.length ] );

//            List<Animator> animatorList = new ArrayList<>( );
//
//            Animator aniXPos = ObjectAnimator.ofFloat( mTvTest, "x", 0, 200 );
//            aniXPos.setDuration( 2000 );
//            animatorList.add( aniXPos );

//            PropertyValuesHolder pvhXPos = PropertyValuesHolder.ofFloat( "x", 200 );
//            Animator aniPos = ObjectAnimator.ofPropertyValuesHolder( mTvTest, pvhXPos );
//            aniPos.setDuration( 2000 );
//            animatorList.add( aniPos );
//
//            Animator aniYPos = ObjectAnimator.ofFloat( mTvTest, "y", 0, 100 );
//            aniYPos.setDuration( 1000 );
//            animatorList.add( aniYPos );
//
//            Animator aniRotate = ObjectAnimator.ofFloat( mTvTest, "rotation", 0, -360 );
//            aniRotate.setDuration( 2000 );
//            animatorList.add( aniRotate );
//
//            AnimatorSet animatorSet = new AnimatorSet( );
//            animatorSet.playTogether( animatorList );
//            animatorSet.start( );

//            TranslateAnimation translateAnimation = new TranslateAnimation( 0, 200, 0, 100 );
//            translateAnimation.setDuration( 2000 );
//            translateAnimation.setFillEnabled( true );
//            translateAnimation.setFillAfter( true );
//            mTvTest.startAnimation( translateAnimation );
        } );

    }
}
