package site.sishidadao.morphtext;

import android.widget.TextView;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor( suppressConstructorProperties = true )
public class MorphChar {
    public TextView TextViewRef;

    public char Char;
    public int FromIdx = -1;
    public int ToIdx = -1;

    public CharLimbo.LimboType getLimboType( ) {
        if ( FromIdx == -1 && ToIdx != -1 )
            return CharLimbo.LimboType.Enter;
        else if ( FromIdx != -1 && ToIdx != -1 )
            return CharLimbo.LimboType.Shift;
        else
            return CharLimbo.LimboType.Leave;
    }
}
